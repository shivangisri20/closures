// function should return a function that invokes cb only 'n' number of times
function limitFunctionCallCount(callbackFunc, n) {
  
 
  return function invokeCb (){
           
    if(n>0){
         let validInvoke = callbackFunc();
         n--;
           
         return validInvoke;
        }
      
      else 
      console.log('null')
      return null ;
        }

}

module.exports = limitFunctionCallCount;